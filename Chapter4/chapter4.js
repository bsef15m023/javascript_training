//Snippet1
var obj = {
    id: "An object",
    f1: function() {
    console.log(this);
    }
};
obj.f1();




//Snippet2
function f1() {
    console.log(this.toString());
    console.log(this == global);
}
f1();

//Snippet3

function f1(a, b) {
    console.log(this, a, b);
}

var obj1 = { id: "Foo"};
f1.call(obj1, 'A', 'B');

var obj2 = { id: "Bar"};
f1.apply(obj2, [ 'A', 'B' ]);


//Snippet4

var obj = {
    id: "xyz",
    printId: function() {
    console.log('The id is '+ this.id + ' '+ this.toString());
    }
};
setTimeout(obj.printId,100);


//Snippet5
var obj = {
id: "xyz",
printId: function() {
console.log('The id is '+ this.id + ' '+ this.toString());
}
};
setTimeout(function() { obj.printId() }, 100);
var callback = function() { obj.printId() };
callback();



//Snippet6
var obj = {
    items: ["a", "b", "c"],
    process: function() {
    var self = this; 
    
    this.items.forEach(function(item) {
        self.print(item);
    });

},
print: function(item) {
    console.log('*' + item + '*');
    }
};
obj.process();



//Snippet7

for(var i = 0; i < 5; i++) {
    console.log(i);
}



//Snippet8

for(var i = 0; i<5; i++) {
    setTimeout(function() {
    console.log(i);
    }, 100);
    if(i==4)
    {
        i=8;
    }
}

//Snippet9
var data = [];
for (var i = 0; i < 5; i++) {
    data[i] = function foo() {
    console.log(i);
    };
}
i=7;
data[0](); data[1](); data[2](); data[3](); data[4]();

//Snippet10
var a = "foo";
function parent() {
    var b = "bar";
    function nested() {
    console.log(a);
    console.log(b);
    }
    nested();
}
parent();


//Snippet11
var a = "foo";
function parent() {
var b = "bar";
}
function nested() {
console.log(a);
console.log(b);
}
parent();
nested();



//Snippet12
var a = "foo";
function grandparent() {
var b = "bar";
	function parent() {
		function nested() {
			console.log(a);
			console.log(b);
		}
		nested();
	}
	parent();
}
grandparent();




//Snippet13
var a = "foo";
function grandparent() {
var b = "bar";
    function parent() {
    var b = "b redefined!";
    function nested() {
        console.log(a);
        console.log(b);
    }
    nested();
}
parent();
}
grandparent();





//Snippet14
for(var i = 0; i < 5; i++) {
    (function() {
    var j = i;
    setTimeout( function() { console.log(j); }, 500*i);
    })();
}

//Snippet15
var a = {"foo":"bar", "Mango":"2", "3":"1"};
for(var i in a) {
console.log(i);
};

//Snippet16
var a = [
    { key: 'foo', val: 'bar'},
    { key: '2', val: '2' },
    { key: '1', val: '1' }
    ];
for(var i in a) {
	console.log(a[i])
};

