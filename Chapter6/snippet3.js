//Shared Variable Example
var total=0;

function Foo(){
    total++;
}

Foo.prototype.getTotal=()=>{
    return total;
}

var f=new Foo();
console.log(f.getTotal());

var f1=new Foo();
console.log(f1.getTotal());