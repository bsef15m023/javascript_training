//How to make a class in JavaScript

function Foo(bar){
    this.bar=bar;
    this.baz='baz';
}

Foo.prototype.fooBar=function(){
    console.log("I am Foo Bar",this.bar);
}

var object=new Foo("Hello");

object.fooBar();