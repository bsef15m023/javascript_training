//DO NOT ASSIGN NON-PRIMITIVE DATA MEMBERS TO PROTOTYPE
function Foo(name){
    this.name=name;
}

Foo.prototype.data=[1,2,3];

Foo.prototype.showData= function(){
    console.log(this.name,this.data);
}

var f=new Foo("Tahir");

var f1=new Foo("Shaheer");

f.showData();
f1.showData();

f.data.push(5);
f.showData();
f1.showData();
