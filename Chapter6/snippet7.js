function Animal(name){
    this.name=name;
}
Animal.prototype.move=function(distance){
    console.log(this.name+" moved "+distance+ " meters");
}

function Snake(){
    Animal.apply(this,Array.prototype.slice.call(arguments));
}

Snake.prototype=new Animal();

Snake.prototype.move=function(){
    console.log("Slithering");
    Animal.prototype.move.call(this,5);
}

var s=new Snake("Sammy The python");
s.move();