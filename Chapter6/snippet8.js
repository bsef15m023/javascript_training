function Foo() { }
Foo.prototype.bar = function() { };

Foo.prototype.baz = function() {
    console.log("Hello");
};

Foo.mixin = function(destObject){
    ['bar', 'baz'].forEach(function(property) {
    destObject.prototype[property] = Foo.prototype[property];
    });

};


function Bar() { };

Bar.prototype.qwerty = function() {};

Foo.mixin(Bar);

var b=new Bar();
b.baz();