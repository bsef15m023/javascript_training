function async(arg,callback){
    console.log('Do something with args '+arg+'  return 1 sec later');
    
    setTimeout(function(){
        
        callback(arg*2);

    }, 1000);

}

var results=[];

var items=[1,2,3,4,5,6];

var limit=2;

var running=0;


function final(){
    console.log("Done",results);
}

function launcher(){
    
    while(items.length>0 && running<limit){
        var item=items.shift();
        async(item,function(result){
            results.push(result);
            running--;
            if(items.length>0){
                launcher();
            }else if(running==0){
                final();
            }
        });
        running++;
    }
}
launcher();