
function async(arg,callback){
    console.log('Do something with args '+arg+'  return 1 sec later');
    
    setTimeout(function(){
        
        callback(arg*2);

    }, 1000);

}

var results=[];

var items=[1,2,3,4,5,6];

function final(){
    console.log("Done",results);
}

items.forEach(function(item){
    async(item,function(result){
        results.push(result);
        if(results.length==items.length){
            final();
        }
    });
});