//Snchronous function writing file
var fs = require('fs');
var data = fs.readFileSync('./index.html', 'utf8');
console.log(data);


//Asynchronous function
fs.readFile('./index.html', 'utf8', function(err, data) {
    console.log(data);
});
//Writing a file in synchronous way
fs.writeFile('./results.txt', 'Hi World', function(err) {
    if(err) throw err;
    console.log('File write completed');
 });
//Writting file in parts
 fs.open('./data/index.html', 'w', function(err, fd) {
    if(err) throw err;
    var buf = new Buffer('bbbbb\n');
    fs.write(fd, buf, 0, buf.length, null, function(err, written, buffer) {
        
        if(err) throw err;
        
        console.log(err, written, buffer);

        fs.close(fd, function() {
            console.log('Write Done');
        });
    });
})

//Opening file read it and than closing it (in parts)
fs.open('./data/index.html', 'r', function(err, fd) {
    if(err) throw err;
    var str = new Buffer(6);
    fs.read(fd, str, 0, str.length, null, function(err, bytesRead, buffer) {
        
        if(err) throw err;
        console.log("---------------Its a read function---------");     
        console.log(err, bytesRead, buffer);
        console.log("-------------------------------------------");
        fs.close(fd, function() {
            console.log('Read Done');
        });
    });
});

//Directory Handling
var path = './data/';
fs.readdir(path, function (err, files) {
    if(err) throw err;
    files.forEach(function(file) {
        
        console.log(path+file);
        
        fs.stat(path+file, function(err, stats) {
            console.log(stats);
        
        });
    });
});

//Creating and deleting directory
fs.mkdir('./newdir', 0666, function(err) {
    
    if(err) throw err;
    
    console.log('Created newdir');
    
   fs.rmdir('./newdir', function(err) {
        if(err) throw err;
        console.log('Removed newdir');
    });
});

//Reading a file through stream and writing it in another file

var file = fs.createReadStream('./data/results.txt', {flags: 'r'} );
var out = fs.createWriteStream('./data/results2.txt', {flags: 'w'});
file.on('data', function(data) {
    console.log('data', data);
    out.write(data);
});
file.on('end', function() {
    console.log('end');
    out.end(function() {
        console.log('Finished writing to file');
    });
});

//Use of pipe with streams
var file = fs.createReadStream('./data/results.txt', {flags: 'r'} );
var out = fs.createWriteStream('./data/results2.txt', {flags: 'w'});
file.pipe(out);

//Append To a file

var file = fs.createWriteStream('./data/results.txt', {flags: 'a'} );
file.write('HELLO! I am appending some text to file\n');
file.end(function() {
    console.log("Append test completed");
});