
//Snippet1

var arr = [ 'a', 'b', 'c'];
arr.push('d');
console.log(arr); 
console.log(arr.pop()); 
console.log(arr);

//Snippet2
var arr = [ 'a', 'b', 'c'];
arr.unshift('1'); 
console.log(arr); 
console.log(arr.shift()); 
console.log(arr); 

//Snippet3
var obj = { has_thing: true, id: 123 };
 if(obj.has_thing) {
  console.log('true', obj.id);
 }



//Snippet4

function process(argv) {
    if(argv.indexOf('help')) {
      console.log('This is the help text.');
    }
}
process(['foo', 'bar', 'help']);


//Snippet5

var arr = ["1", "2", "3"];
console.log(arr.indexOf(2)); 


//Snippet6

console.log(2 == "2"); 
console.log(2 === "2"); 
var arr = ["1", "2", "3"];
console.log(arr.indexOf(2)); 
console.log(arr.indexOf("2")); 


//Snippet7

var lookup = { 12: { foo: 'b'}, 13: { foo: 'a' }, 14: { foo: 'c' }};
console.log(Object.keys(lookup).indexOf(12) > -1); 
console.log(Object.keys(lookup).indexOf(''+12) > -1); 


//Snippet8


var names = ['a', 'b', 'c'];
names.forEach(function(value) {
  console.log(value);
});



//Snippet9


var items = [ { id: 1 }, { id: 2}, { id: 3}, { id: 4 }];

items = items.filter(function(item){
  
    return (item.id % 2 == 0);

});
console.log(items);



//SNIPPET10

var types = ['text/html', 'text/css', 'text/javascript'];
var string = 'text/javascript; encoding=utf-8';
if (types.some(function(value) {

    return string.indexOf(value) > -1;

    })) {
        console.log('The string contains one of the content types.');
    }


//SNIPPET11


var a = [ 'a', 'b', 'c' ];
var b = [ 1, 2, 3 ];

console.log( a.concat(['d', 'e', 'f'], b) );

console.log( a.join('! ') );

console.log( a.slice(1, 3) );

console.log( a.reverse() );

console.log( ' --- ');

var c = a.splice(0, 2);

console.log( a, c );

var d = b.splice(1, 1, 'foo', 'bar');

console.log( b, d );


   
//SNIPPET12

var keys = Object.keys({ Hello: 'foo', Hi: 'bar',Bye:"Bar1"});
console.log(keys, keys.length);


//SNIPPET12-A

var group={Hello:{a:"a",;2b:"b"},Hi:{a1:"1",a2:"2"}};

var keys=Object.keys(group);

keys.forEach(function(Person){
    
    var items=Object.keys(group[Person]);

    items.forEach(function (item){
        console.log(group[Person][item]);
    });

});

//SNIPPET13


var arr = [
    { item: 'Xylophone' },
    { item: 'Carrot' },
    { item: 'Apple'}
];
arr = arr.sort(function (a, b) {
    return a.item.localeCompare(b.item);
});
console.log( arr )




//SNIPPET14


var obj = { a: "v", b: false };
console.log( !!obj.a );



//SNIPPET15

function match(item, filter) {
    var keys = Object.keys(filter);
    // true if any true
    return keys.some(function (key) {
    return item[key] == filter[key];
});
}
var objects = [ 
    { a: 'a', b: 'b', c: 'c'},
    { b: '2', c: '1'},
    { d: '3', e: '4'},
    { e: 'f', c: 'c'} ];
    objects.forEach(function(obj) {
    console.log('Result: ', match(obj, { c: 'c', d: '3'}));
});

//SNIPPET16

var obj = { x: '1', a: '2', b: '3'};
var items = Object.keys(obj);
items.sort(); 
items.forEach(function(item) {
  console.log(item + '=' + obj[item]);
});
 


//SNIPPET17

console.log( doSomething() );
function doSomething() {
     return 'doSomething' ;
}
var doSomethingElse = function() {
     return 'doSomethingElse'; 
}
console.log( doSomethingElse() );


//SNIPPET18

function doSomething() {
     return doSomething.value + 50;
}

var doSomethingElse = function() {
     return doSomethingElse.value + 100;
}

doSomething.value = 100;
doSomethingElse.value = 100;

console.log( doSomething() );
console.log( doSomethingElse() );


//SNIPPET19

var doSomethingElse = function(a, b) {
    console.log(a, b);
    console.log(arguments[0]);
};
doSomethingElse(1, 2, 3, 'foo');

//SNIPPET20

var obj = { hello: 'world', data: [ 1, 2, 3 ] };
console.log(JSON.stringify(obj));


//SNIPPET21

var obj = { a: "value", b: false };
console.log( !!obj.nonexistent );
console.log( 'nonexistent' in obj );
console.log( obj.hasOwnProperty('nonexistent') );
console.log( !!obj.a );
console.log( 'a' in obj );
console.log( obj.hasOwnProperty('a') );


//SNIPPET22

var obj = { a: "value", b: false };
console.log( !!obj.b );
console.log( 'b' in obj );
console.log( obj.hasOwnProperty('b') );

//SNIPPET23

var obj = { a: "value", b: false };
console.log( !!obj.toString );
console.log( 'toString' in obj );
console.log( obj.hasOwnProperty('toString') );


//SNIPPET24

function smallest(){
    return Math.min.apply( Math, arguments);
}
console.log( smallest(999, 899, 99999) );


//SNIPPET25

function test() {
    
    var args = Array.prototype.slice.call(arguments); 
    console.log(args.length);
    console.log(args.concat(['a', 'b', 'c']));
}
test(1, 2, 3);