
//Snippet1

setTimeout(function() { 
    console.log('Timeout ran at M' + new Date().toTimeString());
}, 10000);
setTimeout(function() { 
    console.log('Timeout ran at M      b   ' + new Date().toTimeString());
}, 9000);
setTimeout(function() { 
    console.log('Timeout ran at ' + new Date().toTimeString());
}, 500);


var start = new Date();

var i = 0;

console.log('Enter second loop at: '+start.toTimeString());
while(new Date().getTime() < start.getTime() + 1000)
{
      i++;
}
console.log('Exit second loop at: ' +new Date().toTimeString() +'. Ran '+i+' iterations.');

console.log('Enter loop at: '+start.toTimeString());

while(new Date().getTime() < start.getTime() + 4000)
{
      i++;
}
console.log('Exit loop at: ' +new Date().toTimeString() +'. Ran '+i+' iterations.');


//Snippet2
var http = require('http');

var content = '<html><body><p>Hello World</p><h1>Hello World Again</h1>  </body></html>';

http.createServer(function (request, response) 
{ 
    response.end(content);
}
).listen(8080, 'localhost');

console.log('Server running at http://localhost:8080/.');


//Snippet3
var fs=require('fs');
fs.readFile('/etc/passwd', function(err, data){ console.log(data);});


//Snippet4
setTimeout(function() {
    console.log('setTimeout at '+new Date().toTimeString());
 }, 1);
 
 require('fs').readFile('/etc/passwd', function(err, result) {
   console.log(result);
 } );