//READING FROM FILE

var fs = require('fs');
var file = fs.createReadStream('./test.txt');
file.on('error', function(err) {
    console.log('Error '+err);
    throw err;
});
file.on('data', function(data) {
    console.log('Data '+data);
});
file.on('end', function(){
    console.log('Finished reading all of the data');
});

//WRITING INTO FILE
var fs = require('fs');

var file1 = fs.createWriteStream('./out.txt');

process.stdin.on('data', function(data) {
    file1.write(data);
});

process.stdin.on('end', function() {
    file1.end();
});

process.stdin.resume(); 

//Creating a pipe between read and write file
var fs = require('fs');
file.pipe(fs.createWriteStream('./out1.txt'));
process.stdin.resume();