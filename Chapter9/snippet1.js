setTimeout(function() {
    console.log('Foo');
}, 1000);
    
var counter = 0;
var interval = setInterval( function() {
    
    console.log('Bar', counter);
    
    counter++;

    if (counter >= 3) {
        clearInterval(interval);
    }

}, 1000);