var map = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.json': 'application/json',
    '.css': 'text/css',
    '.png': 'image/png'
};
var ext = '.css';

if(map[ext]) {
    console.log('Content-type', map[ext]);
}