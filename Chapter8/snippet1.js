//SNIPPET 1
//Exporting function from another module

var hello=require('./module.js');
console.log(hello.funcname());

//SNIPPET 2
//Exporting class from another module

var Class=require('./class.js');
var myObj=new Class();
console.log(myObj.myFunc());

//SNIPPET3
//Exporting configuration file which contains shared variable

var config=require('./config.js');
console.log(config.foo);
console.log(config.bar);


//SNIPPET4
//Some Built in variables
console.log(__filename);
console.log(__dirname);
console.log('process.argv', process.argv);
//console.log('process.env', process.env);
if(module === require.main) {
    console.log('This is the main module being run.');
}


