var express=require('express');
var bodyParser=require('body-parser');
var multer=require('multer');

var app=express();
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended:false}));
var tasks=[];

app.get('/',function(req,res){
    res.send('Hello World');
});
app.get('/login.html', function (req, res) {
    res.sendFile( __dirname + "/" + "login.html" );
});
app.get('/addTask', function (req, res) {
   
    var task={
        TaskName:req.query.name,
        TaskPurpose:req.query.purpose
    };
    tasks.push(task);
    res.end(JSON.stringify(tasks));
    
    
 });
 app.get('/showAll', function (req, res) {
    res.end(JSON.stringify(tasks));    
 });
 app.get('/showTask', function (req, res) {
     res.end("<html><head><title>User Information</title></head><body><h1>Name:<h1><p>'"+req.query.name+"'</p><h1>Purpose:<h1><p>'"+req.query.purpose+"'</p></body></html><a href='http://localhost:8080/login.html'>Go back</a>");
 });
 
 

var server=app.listen(8080,function(){
    var host=server.address().address;
    var port=server.address().port;
    console.log("Example app listening at http://%s%s",host,port);
})